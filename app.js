var restify = require('restify');
var builder = require('botbuilder');
var serveStatic = require('serve-static-restify')

// Setup Restify Server
var server = restify.createServer();

server.pre(serveStatic(__dirname + '/static'))

server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());

//=========================================================
// Bots Middleware
//=========================================================

// Anytime the major version is incremented any existing conversations will be restarted.
bot.use(builder.Middleware.dialogVersion({ version: 1.0, resetCommand: /^reset/i }));

//=========================================================
// Bots Global Actions
//=========================================================

bot.endConversationAction('goodbye', 'Goodbye :)', { matches: /^goodbye/i });
bot.beginDialogAction('help', '/help', { matches: /^help/i });

bot.dialog('/', [
    function (session) {
        // Send a greeting and show help.
        session.send("Hi! I'm the bot from Quotemehappy.com");
        session.beginDialog('/menu');
    },
    function (session, results) {
        // Always say goodbye
        session.send("Ok... See you later!");
    }
]);

bot.dialog('/menu', [
    function (session) {
         builder.Prompts.choice(session, "Please let me know from the options below how can we help you?", "Quote|Claim|Help");
    },
    function (session, results) {
        if (results.response && results.response.entity != '(quit)') {
            // Launch demo dialog
            session.beginDialog('/' + results.response.entity);
        } else {
            // Exit the menu
            session.endDialog();
        }
    },
    function (session, results) {
        // The menu runs a loop until the user chooses to (quit).
        session.replaceDialog('/menu');
    }
]).reloadAction('reloadMenu', null, { matches: /^menu|show menu/i });

bot.dialog('/help', [
    function (session) {
        session.endDialog("Global commands that are available anytime:\n\n* menu - Exits a demo and returns to the menu.\n* goodbye - End this conversation.\n* help - Displays these commands.");
    }
]);

bot.dialog('/Claim', [
     function (session) {
        session.send("No worries, I am here to help");
        session.send("In order for us to get going, you'll need to connect your QHM account by entering your User ID and Password on the log in screen. Worry not, human! Your User ID and Password won't be shared with Facebook.");
        session.send("After connecting, you can submit a claim directly through me :). If something does not compute, don't worry, you can always connect with a live Customer Care Professional through the menu");
        var msg = createSigninCard(session);
        session.send(msg);
        session.send("Thanks for logging in and happy to confirm you have an eligable home policy for this service :)");
         builder.Prompts.confirm(session, "Are you claiming for just one item?", 'One Item|More than One Item');
    },
    function (session, results) {
        if (!results.response) {
            session.send("Since your claiming for more than one item, you are going to need to call us. Our direct claims number is 0345 030 6902.");
            session.endDialog();
        }
        else{
            session.send("Cool :)");
           builder.Prompts.choice(session, "How much do you think the item is worth?", 'Less than £2,000|Over £2,000');
        }
    },
    function (session, results) {
        if (!results.response){
            session.send("Understood. As your item is worth more than £2,000 you are going to need to call us. Our direct claims number is 0345 030 6902.");
            session.endDialog();
        }
        else{
        session.send("Great, just a few more questions");
            builder.Prompts.choice(session, "What are you making a claim for?", "Smartphone|Tablet|Laptop|Other")
        }
    },
    function (session, results, next) {
        session.userData.ClaimDetailsItem = results.response.entity
        next();
    },
    function (session) {
        builder.Prompts.text(session, "Can you tell us the make and model of your item? E.g., iPhone 6s")
    },
    function (session, results) {
        session.userData.ClaimDetailsMakeModel = results.response
        builder.Prompts.choice(session, "Is your phone lost, stolen or damaged?", "Lost|Stolen|Damaged")
    },
    function (session, results) {
        session.userData.ClaimDetailsType = results.response.entity
        builder.Prompts.confirm(session, "Did this happen at home?", 'Yes|no');
    },
    function (session, results) {
        if (!results.response){
            session.send("Please call us on 0345 030 6902 for this type of claim");
            session.endDialog();
        }
        else{
            session.send("Brilliant! nearly there");
            builder.Prompts.text(session, "Can you please give a brief description of what happened, e.g., I dropped my phone and cracked the screen")
        }
    },
    function (session, results) {
        session.userData.ClaimDetailsWhatHappened = results.response
        session.send("Thanks for letting me know. Last question");
        
        var cards = getPaymentOptions();

        // create reply with Carousel AttachmentLayout
        var reply = new builder.Message(session)
           .attachmentLayout(builder.AttachmentLayout.carousel)
           .attachments(cards);

        builder.Prompts.choice(session, msg, "select:BankAccount|select:Amazon|select:ItemReplacment");  
    },
    function (session, results) {
        session.send(results.response.entity);
        next(); 
    },    
    function (session) {
        session.userData.ClaimDetailsPayment = results.response.entity
        session.send('OK, please check the details you have given me are correct: ');
        session.send("Claiming for:  " + session.userData.ClaimDetailsMakeModel);
        session.send('Claim Type: ' + session.userData.ClaimDetailsType);
        session.send("Compensation:  " + session.userData.PaymentType);
        builder.Prompts.choice(session, "Confirm", 'Submit claim|No, I need to change something');
    },
]);

function createSigninCard(session) {
    return new builder.Message(session)
      .sourceEvent({
        facebook: {
          attachment: {
            type: 'template',
            payload: {
              template_type: 'generic',
              elements: [{
                title: 'Welcome to Quote Me Happy',
                image_url: 'http://claimsjourney.azurewebsites.net/linking.png',
                buttons: [{
                  type: 'account_link',
                  url: 'http://claimsjourney.azurewebsites.net/index.html'
                }]
              }]
            }
          }
        }
      });
}

function getPaymentOptions(session) {
    return [
        new builder.HeroCard(session)
            .title('Money to your bank account')
            .text('We will transfer the funds to the account stores on the policy')
            .images([
                builder.CardImage.create(session, 'http://claimsjourney.azurewebsites.net/Carousel_Money_to_your_bank_account_asset.png')
            ])
            .buttons([
                builder.CardAction.imBack(session, "select:BankAccount", "Select Option")
            ]),
        new builder.HeroCard(session)
            .title('Amazon Voucher')
            .text('We will email you a voucher with the items value preloaded')
            .images([
                builder.CardImage.create(session, 'http://claimsjourney.azurewebsites.net/Carousel_Amazon_Voucher_asset.png')
            ])
            .buttons([
                builder.CardAction.imBack(session, "select:Amazon", "Select Option"),
            ]),
        new builder.HeroCard(session)
            .title('Item Replacement')
            .text('We will send a replacement of your item to your insured address')
            .images([
                builder.CardImage.create(session, 'http://claimsjourney.azurewebsites.net/Carousel_Item_replacement_asset.png')
            ])
            .buttons([
                builder.CardAction.imBack(session, "select:ItemReplacment", "Select Option"),
            ])                        
    ];
}